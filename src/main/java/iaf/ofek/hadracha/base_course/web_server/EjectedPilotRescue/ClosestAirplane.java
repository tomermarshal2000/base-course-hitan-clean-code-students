package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.AirSituation.Airplane;
import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;
import iaf.ofek.hadracha.base_course.web_server.Utilities.GeographicCalculations;

public class ClosestAirplane {
    private Airplane closestAirplane;
    private double closestDistance;

    public void updateIfCloser(GeographicCalculations geographicCalculations, Airplane airplane, Coordinates closeTo) {
        double distance = geographicCalculations.distanceBetween(airplane.coordinates, closeTo);

        if (isThisTheFirstAirplane() || distance < closestDistance) {
            closestDistance = distance;
            closestAirplane = airplane;
        }
    }

    private boolean isThisTheFirstAirplane() {
        return closestAirplane == null;
    }

    public Airplane getClosestAirplane() {
        return closestAirplane;
    }
}
