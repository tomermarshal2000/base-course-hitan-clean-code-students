package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectedPilotRescueRestController {
    private final CrudDataBase dataBase;
    private final AirplanesAllocationManager airplanesAllocationManager;

    public EjectedPilotRescueRestController(CrudDataBase dataBase, AirplanesAllocationManager airplanesAllocationManager) {
        this.dataBase = dataBase;
        this.airplanesAllocationManager = airplanesAllocationManager;
    }

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getInfos() {
        return dataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("/takeResponsibility")
    public void takeResponsibility(@RequestParam("ejectionId") int ejectionId, @CookieValue("client-id") String clientId) {
        EjectedPilotInfo ejectedPilotInfo = dataBase.getByID(ejectionId, EjectedPilotInfo.class);

        if (ejectedPilotInfo == null) {
            throw new IllegalArgumentException("No such ejection");
        }

        if (ejectedPilotInfo.rescuedBy != null) {
            return;
        }

        ejectedPilotInfo.rescuedBy = clientId;
        dataBase.update(ejectedPilotInfo);
        airplanesAllocationManager.allocateAirplanesForEjection(ejectedPilotInfo, clientId);
    }
}